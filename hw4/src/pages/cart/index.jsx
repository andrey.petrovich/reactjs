import React from "react";
import styles from "../../styles/cart.module.scss";
import { Product } from "../../components/product";
import { useSelector } from "react-redux";

const getProducts = (state) => state.products;

export const Cart = () => {

  const { items, productsInCart } = useSelector(getProducts);

  const productsCart = items.filter((product) =>
    productsInCart.find((item) => item.id === product.id)
  );

  const emptyCart = !productsCart.length;

  return (
    <div className={styles.Cart}>
      {emptyCart && <h1>Ви ще нічого не додали</h1>}
      {!emptyCart && (
        <ul className={styles.CartList}>
          {productsCart.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

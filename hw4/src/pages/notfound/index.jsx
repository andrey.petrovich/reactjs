import React from "react";
import styles from "../../styles/notfound.module.scss";

export function Notfound() {
  return <div className={styles.Notfound}>Not found</div>;
}

import React from "react";
import styles from "../../styles/favorites.module.scss";
import { Product } from "../../components/product";
import { useSelector } from "react-redux";

const getProducts = (state) => state.products;

export const Favorites = () => {
  
  const { items, favorites } = useSelector(getProducts);
  
  const productsFavorites = items.filter((product) =>
    favorites.includes(product.id)
  );

  const emptyFavorites = !productsFavorites.length;

  return (
    <div className={styles.Favorites}>
      {emptyFavorites && <h1>Ви ще нічого не вибрали</h1>}
      {!emptyFavorites && (
        <ul className={styles.FavoritesList}>
          {productsFavorites.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

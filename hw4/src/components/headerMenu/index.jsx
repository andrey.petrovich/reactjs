import React from "react";
import { NavLink } from "react-router-dom";
import styles from "../../styles/headerMenu.module.scss";

export const HeaderMenu = () => {
  return (
    <nav className={styles.HeaderMenu}>
      <ul className={styles.HeaderMenu__list}>
        <li className={styles.HeaderMenu__item}>
          <NavLink to="/">Головна</NavLink>
        </li>
        <li className={styles.HeaderMenu__item}>
          <NavLink to="/cart">Кошик</NavLink>
        </li>
        <li className={styles.HeaderMenu__item}>
          <NavLink to="/favorites">Вибране</NavLink>
        </li>
      </ul>
    </nav>
  );
};

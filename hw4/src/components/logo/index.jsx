import React from "react";
import styles from "../../styles/logo.module.scss";

export const Logo = () => {
  return (
    <a href="" className={styles.LogoLink}>
      <img src="./img/logo1.png" alt="logo" />
    </a>
  );
};

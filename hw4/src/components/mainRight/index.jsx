import React from "react";
import { Outlet } from "react-router-dom";
import styles from "../../styles/mainRight.module.scss";

export const MainRight = () => {
  return (
    <div className={styles.MainRight}>
      <Outlet />
    </div>
  );
};

import React from "react";
import styles from "../../styles/footer.module.scss";

export const Footer = () => {
  return <div className={styles.Footer__row}>&copy; Iphone</div>;
};

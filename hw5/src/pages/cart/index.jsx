import React from "react";
import { useSelector } from "react-redux";
import { FormBuy } from "../../components/formBuy/formBuy";
import { Product } from "../../components/product";
import styles from "../../styles/cart.module.scss";

const getProducts = (state) => state.products;

export const Cart = () => {
  const { items, productsInCart } = useSelector(getProducts);
  const productsCart = items.filter((product) =>
    productsInCart.find((item) => item.id === product.id)
  ); 
  const emptyCart = !productsCart.length;

  return (
    <div className={styles.Cart}>
      {emptyCart && <h1>Товари в кошику відсутні</h1>}
      {!emptyCart && <FormBuy productsCart={productsCart} />}
      {!emptyCart && (
        <ul className={styles.CartList}>
          {productsCart.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

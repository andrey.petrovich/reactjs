import React from "react";
import styles from "../../styles/main.module.scss";
import { MainRight } from "../mainRight";

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <MainRight />
    </div>
  );
};

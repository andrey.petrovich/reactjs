import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "../../styles/myModal.module.scss";
import { Button } from "../button";
import { closeModal } from "../../redux/reducers/modal";

const getModalState = (state) => state.modal;

export function MyModal() {
  const dispatch = useDispatch();
  const { isOpen, header, closeButton, name, price, action } =
    useSelector(getModalState);
  const handleModalClose = () => dispatch(closeModal());
  const rootClasses = [styles.myModal];
  if (isOpen) rootClasses.push(styles.active);

  return (
    <div className={rootClasses.join(" ")} onClick={handleModalClose}>
      <div
        className={styles.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={styles.myModalHeader}>
          <h1 className={styles.myModalTitle}>{header}</h1>
          {closeButton && (
            <div className={styles.myModalCloseBtn} onClick={handleModalClose}>
              ✖
            </div>
          )}
        </div>
        <div className={styles.myModalBody}>
          <div className={styles.modalAddCart}>
            <div className={styles.title}>{name}</div>
            <div className={styles.price}>
              <span className={styles.sum}>{price}</span>
              <span className={styles.currency}> грн</span>
            </div>
          </div>
        </div>
        <div className={styles.myModalAction}>
          {action.map((btn) => (
            <Button
              key={btn.text}
              text={btn.text}
              backgroundColor={btn.backgroundColor}
              onClick={() => {
                btn.actionCart && dispatch(btn.actionCart);
                handleModalClose();
              }}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

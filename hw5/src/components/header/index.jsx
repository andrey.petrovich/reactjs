import React from "react";
import styles from "../../styles/header.module.scss";
import { useSelector } from "react-redux";
import { HeaderMenu } from "../headerMenu";
import { Logo } from "../logo";
import { FavIco } from "../icons/favIco";
import { CartIco } from "../icons/cartIco";

const getProducts = (state) => state.products;
export const Header = () => {
  
  const { favorites, productsInCart } = useSelector(getProducts);

  const countFavorites = favorites.length;
  const countProductsInCart = productsInCart.reduce(
    (value, item) => value + item.count,
    0
  );

  return (
    <div className={styles.Header__row}>
      <Logo />
      <HeaderMenu />

      <div className={styles.HeaderActions}>
        <div className={styles.HeaderCart}>
          <CartIco width={26} fill={"darkred"} />
          <span className={styles.cartValue}>{countProductsInCart}</span>
        </div>
        <div className={styles.HeaderFavorites}>
          <FavIco width={26} fill={"#ffda12"} />
          <span className={styles.favoritesValue}>{countFavorites}</span>
        </div>
      </div>
    </div>
  );
};

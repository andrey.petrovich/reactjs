import { createSlice } from "@reduxjs/toolkit";
import { actionBuy } from "../extraReducers/actionBuy";

const initialState = {
  status: false,
  name: "",
  lastName: "",
  age: 0,
  address: "",
  phone: "",
  items: [],
};

const formBuySlice = createSlice({
  name: "formBuy",
  initialState,
  reducers: {
    clearStatus: (state, action) => {
      state.status = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(actionBuy.fulfilled, (state, action) => {
      const { name, lastName, age, address, phone, items } = action.payload;
      return {
        ...state,
        status: true,
        name,
        lastName,
        age,
        address,
        phone,
        items,
      };
    });
    builder.addCase(actionBuy.rejected, (state, action) => {
      console.log("error", action.payload);
    });
  },
});

export const { clearStatus } = formBuySlice.actions;
export default formBuySlice.reducer;

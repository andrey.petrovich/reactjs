import { createSlice } from "@reduxjs/toolkit";
import { getData } from "../extraReducers/getData";
import { handleFavoritesClick } from "../extraReducers/handleFavoritesClick";

export const initialState = {
  items: [],
  favorites: [],
  productsInCart: [],
  error: null,
  status: null,
};

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
 
    setFavorites: (state, action) => {
      state.favorites = action.payload;
    },
 
    setProductsInCart: (state, action) => {
      state.productsInCart = action.payload;
    },

    toggleFavorites: (state, action) => {
      if (state.favorites.includes(action.payload)) {
        state.favorites = state.favorites.filter((id) => id !== action.payload);
      } else {
        state.favorites.push(action.payload);
      }
    },

    handleAddCartClick: (state, action) => {
    
      const itemFound = state.productsInCart.some(
        (item) => item.id === action.payload
      );
      if (itemFound) {
        state.productsInCart.find(
          (item) => item.id === action.payload
        ).count += 1;
      } else {
        state.productsInCart.push({ id: action.payload, count: 1 });
      }
      localStorage.setItem("Cart", JSON.stringify(state.productsInCart));
    },

    handleDeleteCartClick: (state, action) => {

      const itemIndex = state.productsInCart.findIndex(
        (item) => item.id === action.payload
      );

      if (itemIndex !== -1) {
        if (state.productsInCart[itemIndex].count > 1) {
          state.productsInCart[itemIndex].count -= 1;
        } else {
          state.productsInCart.splice(itemIndex, 1);
        }
        localStorage.setItem("Cart", JSON.stringify(state.productsInCart));
      }
    },
  },

  extraReducers: (builder) => {
    builder.addCase(getData.rejected, (state, action) => {
      state.error = action.payload;
      state.status = "rejected";
    });
    builder.addCase(getData.pending, (state, action) => {
      state.error = null;
      state.status = "pending";
    });
    builder.addCase(getData.fulfilled, (state, action) => {
      state.error = null;
      state.status = "fulfilled";
      state.items = action.payload;
    });
    builder.addCase(handleFavoritesClick.fulfilled, (state, action) => {});
    builder.addCase(handleFavoritesClick.rejected, (state, action) => {
      console.log("error", action.payload);
    });
  },
});

export const {
  setFavorites,
  setProductsInCart,
  handleAddCartClick,
  handleDeleteCartClick,
  toggleFavorites,
} = productsSlice.actions;
export default productsSlice.reducer;

import { configureStore } from "@reduxjs/toolkit";
import products from "../reducers/productsThunk";
import modal from "../reducers/modalReducer";
import formBuy from "../reducers/formBuyThunk";

export const store = configureStore({
  reducer: {
    products,
    modal,
    formBuy,
  },
});

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Field, Form } from "formik";
import { validationSchema } from "./schemas";
import InputMask from "react-input-mask";
import { actionBuy } from "../../redux/extraReducers/actionBuy";
import { selectorGetProducts } from "../../redux/selectors/selectors";
import styles from "./FormBuy.module.scss";

export const FormBuy = ({ productsCart }) => {
  const dispatch = useDispatch();
  const { productsInCart } = useSelector(selectorGetProducts);
  const modifyProductsArray = productsCart.map((product) => {
    return {
      ...product,
      count: productsInCart.find((item) => item.id === product.id).count,
    };
  });

  const onSubmit = async (values, actions) => {
    dispatch(actionBuy({ ...values, items: modifyProductsArray }));

    await new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
    actions.setSubmitting(false);
    actions.resetForm();
  };

  const initialValues = {
    name: "",
    lastName: "",
    age: "",
    address: "",
    phone: "",
  };

  return (
    <div className={styles.FormBuy}>
      <h1>Хочете придбати товар?</h1>
      <div>
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {({ errors, touched }) => (
            <Form className={styles.FormBody} autoComplete="off">
              <div className={styles.FieldContainer}>
                <Field
                  className={
                    errors.name && touched.name ? `${styles.inputError}` : ""
                  }
                  type="text"
                  name="name"
                  placeholder="Ваше ім'я"
                />
                {errors.name && touched.name ? (
                  <div className={styles.messageError}>{errors.name}</div>
                ) : null}
              </div>
              <div className={styles.FieldContainer}>
                <Field
                  className={
                    errors.lastName && touched.lastName
                      ? `${styles.inputError}`
                      : ""
                  }
                  type="text"
                  name="lastName"
                  placeholder="Ваше Прізвище"
                />
                {errors.lastName && touched.lastName ? (
                  <div className={styles.messageError}>{errors.lastName}</div>
                ) : null}
              </div>

              <div className={styles.FieldContainer}>
                <Field
                  className={
                    errors.age && touched.age ? `${styles.inputError}` : ""
                  }
                  type="text"
                  name="age"
                  placeholder="Ваш вік"
                />
                {errors.age && touched.age ? (
                  <div className={styles.messageError}>{errors.age}</div>
                ) : null}
              </div>

              <div className={styles.FieldContainer}>
                <Field
                  className={
                    errors.address && touched.address
                      ? `${styles.inputError}`
                      : ""
                  }
                  type="text"
                  name="address"
                  placeholder="Адреса доставки"
                />
                {errors.address && touched.address ? (
                  <div className={styles.messageError}>{errors.address}</div>
                ) : null}
              </div>

              <div className={styles.FieldContainer}>
                <Field name="phone">
                  {({ field }) => (
                    <InputMask
                      {...field}
                      className={
                        errors.phone && touched.phone
                          ? `${styles.inputError}`
                          : ""
                      }
                      mask="(***)***-**-**"
                      placeholder="Мобільний номер"
                    />
                  )}
                </Field>
                {errors.phone && touched.phone ? (
                  <div className={styles.messageError}>{errors.name}</div>
                ) : null}
              </div>

              <button className={styles.FormSubmit} type="submit">
                Придбати
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

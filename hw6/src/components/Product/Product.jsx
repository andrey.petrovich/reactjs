import React, { useContext } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import {
  handleAddCartClick,
  handleDeleteCartClick,
} from "../../redux/reducers/productsThunk";
import { handleFavoritesClick } from "../../redux/extraReducers/handleFavoritesClick";
import { openModal } from "../../redux/reducers/modalReducer";
import { FavIco } from "../../UI/Icons/FavIco/favIco";
import { FavEmptyIco } from "../../UI/Icons/FavEmptyIco/favEmptyIco";
import { MinusIco } from "../../UI/Icons/MinusIco/minusIco";
import { PlusIco } from "../../UI/Icons/PlusIco/plusIco";
import { selectorGetProducts } from "../../redux/selectors/selectors";
import { ToggleProductsViewContext } from "../../contexts/contexts";
import styles from "./Product.module.scss";

export const Product = ({ product }) => {
  const dispatch = useDispatch();
  const { toggleView } = useContext(ToggleProductsViewContext);
  const classView = toggleView ? styles.grid : styles.layout;
  const { favorites, productsInCart } = useSelector(selectorGetProducts);
  const { id, name, price, imgUrl, article, color } = product;
  const isFavorites = favorites.includes(id);
  const searchItem = productsInCart.find((item) => item.id === id);
  const isCart = searchItem?.count;
  const addToCart = dispatch(() => handleAddCartClick(id));
  const deleteToCart = dispatch(() => handleDeleteCartClick(id));

  const handleAddProduct = () => {
    dispatch(
      openModal({
        header: "Хочете додати товар в кошик?",
        closeButton: true,
        name,
        price,
        action: [
          {
            text: "Так",
            backgroundColor: "darkred",
            actionCart: addToCart,
          },
          {
            text: "Ні",
            backgroundColor: "darkred",
            actionCart: null,
          },
        ],
      })
    );
  };

  const handleDeleteProduct = () => {
    dispatch(
      openModal({
        header: "Хочете видалити товар з кошика?",
        closeButton: true,
        name,
        price,
        action: [
          {
            text: "Так",
            backgroundColor: "darkred",
            actionCart: deleteToCart,
          },
          {
            text: "Ні",
            backgroundColor: "darkred",
            actionCart: null,
          },
        ],
      })
    );
  };

  return (
    <>
      <div className={[styles.ProductContainer, classView].join(" ")}>
        <div className={styles.Product}>
          <span className={styles.favorites}>
            {isFavorites && (
              <span
                onClick={() => {
                  dispatch(handleFavoritesClick(id));
                }}
              >
                <FavIco width={25} fill={"#ffda12"} />
              </span>
            )}

            {!isFavorites && (
              <span
                onClick={() => {
                  dispatch(handleFavoritesClick(id));
                }}
              >
                <FavEmptyIco width={20} fill={"#ffda12"} />
              </span>
            )}
          </span>
          <div className={styles.body}>
            <div className={styles.img}>
              <img src={`./img/goods/${imgUrl}`} alt="" />
            </div>
            <div className={styles.title}>{name}</div>
            <div className={styles.article}>
              <span className={styles.articleText}>Артикул</span>:&nbsp;
              <span className={styles.articleValue}>{article}</span>
            </div>
            <div className={styles.color}>
              <span className={styles.colorText}>Колір</span>:&nbsp;
              <span className={styles.colorValue}>{color}</span>
            </div>
          </div>
          <div className={styles.price}>
            <span className={styles.sum}>{price}</span>
            <span className={styles.currency}> грн</span>
          </div>
          <div className={styles.bottom}>
            <div className={styles.actions}>
              <span className={styles.inCartTxt}>Кошик:</span>
              <span className={styles.inCartCount}>{isCart || 0}</span>
              <span className={styles.cart}>
                {isCart && (
                  <span onClick={handleDeleteProduct}>
                    <MinusIco width={20} fill={"darkred"} />
                  </span>
                )}
                <span onClick={handleAddProduct}>
                  <PlusIco width={20} fill={"darkgrey"} />
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,
};

Product.defaultProps = {
  product: {
    name: "",
    price: 0,
    imgUrl: "",
    article: "",
    color: "",
  },
};

import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Layout } from "../../Layout/Layout";
import { getData } from "../../redux/extraReducers/getData";
import {
  setFavorites,
  setProductsInCart,
} from "../../redux/reducers/productsThunk";
import { ToggleProductsViewProvider } from "../../contexts/ToggleProductsViewProvider";

// load Favorites from localStorage
const favoriteIdsFromStorage = localStorage.getItem("favorites");
// load products in Cart from localStorage
const cartIdsFromStorage = localStorage.getItem("Cart");

// initial app
export const App = () => {
  const dispatch = useDispatch();

  /*--------------------LOAD PRODUCTS--------------------*/
  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  /*--------------------MOUNT COMPONENT--------------------*/
  useEffect(() => {
    // load Favorites
    if (favoriteIdsFromStorage) {
      dispatch(setFavorites(JSON.parse(favoriteIdsFromStorage)));
    }
    // load Cart
    if (cartIdsFromStorage) {
      dispatch(setProductsInCart(JSON.parse(cartIdsFromStorage)));
    }
  }, [dispatch]);

  /*--------------------RENDER--------------------*/
  return (
    <ToggleProductsViewProvider>
      <Layout />
    </ToggleProductsViewProvider>
  );
};

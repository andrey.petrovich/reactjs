import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { Product } from "../../components/Product/Product";
import { ToggleProductsViewContext } from "../../contexts/contexts";
import { selectorGetProducts } from "../../redux/selectors/selectors";
import styles from "./Favorites.module.scss";

export const Favorites = () => {
  const { toggleView } = useContext(ToggleProductsViewContext);
  const classView = toggleView ? styles.grid : styles.layout;
  const { items, favorites } = useSelector(selectorGetProducts);
  const productsFavorites = items.filter((product) =>
    favorites.includes(product.id)
  );

  const emptyFavorites = !productsFavorites.length;

  return (
    <div className={styles.Favorites}>
      {emptyFavorites && <h1>Товари відсутні</h1>}
      {!emptyFavorites && (
        <ul className={[styles.FavoritesList, classView].join(" ")}>
          {productsFavorites.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

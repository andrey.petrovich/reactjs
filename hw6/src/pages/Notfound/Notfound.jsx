import React from "react";
import styles from "./Notfound.module.scss";

export function Notfound() {
  return <div className={styles.Notfound}>404 Not found</div>;
}

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../redux/reducers/modalReducer";
import { selectorGetModalState } from "../../redux/selectors/selectors";
import { Button } from "../Button/Button";
import styles from "./MyModal.module.scss";

export function MyModal() {
  const dispatch = useDispatch();

  const { isOpen, header, closeButton, name, price, action } = useSelector(
    selectorGetModalState
  );
  const handleModalClose = () => dispatch(closeModal());
  const rootClasses = [styles.myModal];
  if (isOpen) rootClasses.push(styles.active);
  return (
    <div className={rootClasses.join(" ")} onClick={handleModalClose}>
      <div
        className={styles.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={styles.myModalHeader}>
          <h1 className={styles.myModalTitle}>{header}</h1>
          {closeButton && (
            <div className={styles.myModalCloseBtn} onClick={handleModalClose}>
              ✖
            </div>
          )}
        </div>
        <div className={styles.myModalBody}>
          <div className={styles.modalAddCart}>
            <div className={styles.title}>{name}</div>
            <div className={styles.price}>
              <span className={styles.sum}>{price}</span>
              <span className={styles.currency}> грн</span>
            </div>
          </div>
        </div>
        <div className={styles.myModalAction}>
          {action.map((btn) => (
            <Button
              key={btn.text}
              text={btn.text}
              backgroundColor={btn.backgroundColor}
              onClick={() => {
                btn.actionCart && dispatch(btn.actionCart);
                handleModalClose();
              }}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

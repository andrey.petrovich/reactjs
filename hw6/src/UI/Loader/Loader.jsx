import React from "react";
import styles from "./Loader.module.scss";
import { LoaderIco } from "../Icons/LoaderIco/loaderIco";

export const Loader = () => {
  return (
    <div className={styles.Loader}>
      <span className={styles.fa_loader}>
        <LoaderIco width={50} fill="yellow" />
      </span>
    </div>
  );
};

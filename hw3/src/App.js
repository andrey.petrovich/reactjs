import React, { useState, useEffect } from "react";
import { useFetching } from "./hooks/useFetching";
import { Service } from "./components/services";
import { Layout } from "./layout";

// load Favorites from localStorage
const favoriteIdsFromStorage = localStorage.getItem("favorites");
// load products in Cart from localStorage
const cartIdsFromStorage = localStorage.getItem("Cart");

export const App = () => {
  const [products, setProducts] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const handleFavoritesClick = (id) => {
    if (favorites.includes(id)) {
      setFavorites((state) => state.filter((currentId) => currentId !== id));
    } else {
      setFavorites((state) => [...state, id]);
    }
  };
  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
  }, [favorites]);

  const [productsInCart, setProductsInCart] = useState([]);
  const handleCartClick = (id) => {
    if (productsInCart.includes(id)) {
      setProductsInCart((state) =>
        state.filter((currentId) => currentId !== id)
      );
    } else {
      setProductsInCart((state) => [...state, id]);
    }
  };
  useEffect(() => {
    localStorage.setItem("Cart", JSON.stringify(productsInCart));
  }, [productsInCart]);

  const [fetchData, isDataLoading, fetchError] = useFetching(async () => {
    const data = await Service.getAll();
    setProducts(data.phone);
  });

  useEffect(() => {
    fetchData();
    if (favoriteIdsFromStorage) {
      setFavorites(JSON.parse(favoriteIdsFromStorage));
    }
    if (cartIdsFromStorage) {
      setProductsInCart(JSON.parse(cartIdsFromStorage));
    }
  }, []);

  return (
    <Layout
      isDataLoading={isDataLoading}
      fetchError={fetchError}
      favorites={favorites}
      productsInCart={productsInCart}
      handleFavoritesClick={handleFavoritesClick}
      handleCartClick={handleCartClick}
      products={products}
    />
  );
};

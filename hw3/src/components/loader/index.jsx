import React from "react";
import styles from "../../styles/loader.module.scss";
import { LoaderIco } from "../icons/loaderIco";

export const Loader = () => {
  return (
    <div className={styles.Loader}>
      <span className={styles.fa_loader}>
        <LoaderIco width={50} fill={"#e492ed"} />
      </span>
    </div>
  );
};

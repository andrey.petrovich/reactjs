import React, { useState } from "react";
import PropTypes from "prop-types";
import styles from "../styles/layout.module.scss";
import { Header } from "../components/header";
import { Main } from "../components/main";
import { Footer } from "../components/footer";
import { MyModal } from "../components/myModal";

export const Layout = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInCart,
  handleCartClick,
}) => {
  const [modal, setModal] = useState(false);
  const [modalData, setModalData] = useState({
    header: "",
    closeButton: true,
    name: "",
    price: "",
    action: [
      {
        text: "",
        backgroundColor: "",
        onClick: null,
      },
    ],
  });

  return (
    <>
      <header className={styles.Header}>
        <div className={styles.container}>
          <Header favorites={favorites} productsInCart={productsInCart} />
        </div>
      </header>
      <main className={styles.Main}>
        <div className={styles.container}>
          <Main
            isDataLoading={isDataLoading}
            fetchError={fetchError}
            products={products}
            favorites={favorites}
            handleFavoritesClick={handleFavoritesClick}
            productsInCart={productsInCart}
            handleCartClick={handleCartClick}
            setModal={setModal}
            setModalData={setModalData}
          />
        </div>
      </main>
      <footer className={styles.Footer}>
        <div className={styles.container}>
          <Footer />
        </div>
      </footer>
      <MyModal visible={modal} setVisible={setModal} modalData={modalData} />
    </>
  );
};

Layout.propTypes = {
  products: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInCart: PropTypes.array.isRequired,
  handleCartClick: PropTypes.func.isRequired,
};

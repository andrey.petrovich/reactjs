import React from "react";
import PropTypes from "prop-types";
import { useOutletContext } from "react-router-dom";
import styles from "../../styles/favorites.module.scss";
import { Product } from "../../components/product";

export const Favorites = () => {
  const {
    products,
    favorites,
    handleFavoritesClick,
    productsInCart,
    handleCartClick,
    setModal,
    setModalData,
  } = useOutletContext();

  const productsFavorites = products.filter((product) =>
    favorites.includes(product.id)
  );

  const emptyFavorites = !productsFavorites.length;

  return (
    <div className={styles.Favorites}>
      {emptyFavorites && <h1>Ви ще нічого не вибрали</h1>}
      {!emptyFavorites && (
        <ul className={styles.FavoritesList}>
          {productsFavorites.map((product) => (
            <Product
              key={product.id}
              product={product}
              favorites={favorites}
              handleFavoritesClick={handleFavoritesClick}
              productsInCart={productsInCart}
              handleCartClick={handleCartClick}
              setModal={setModal}
              setModalData={setModalData}
            ></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

Favorites.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInCart: PropTypes.array.isRequired,
  handleCartClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};

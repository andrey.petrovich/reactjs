import styled from "styled-components";

export const MyButton = styled.button`
  cursor: pointer;
  border: none;
  padding: 10px 15px;
  color: #fff;
  text-transform: uppercase;
  border-radius: 5px;
  transition: all 0.3s;
  background-color: ${({ className }) => {
    switch (className) {
      case "red":
        return "#B3382C";
      case "green":
        return "#34AB40";
      case "blue":
        return "#5661E1";
      default:
        return "#E74C3C";
    }
  }};

  &:hover {
    filter: contrast(200%);
  }
`;
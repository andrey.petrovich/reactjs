import React, { Component } from "react";
import styles from "../../styles/header.module.scss";
import PropTypes from "prop-types";

export class Header extends Component {
  render() {
    return (
      <div className={styles.HeaderContainer}>
        <div className={styles.Header}>
          <div className={styles.HeaderCart}>
            <i className="fa-solid fa-basket-shopping"></i>
            <span className={styles.cartValue}>
              {this.props.goodsCartCount}
            </span>
          </div>
          <div className={styles.HeaderFavorite}>
            <i className="fa-solid fa-star"></i>
            <span className={styles.favoriteValue}>
              {this.props.goodsFavoriteCount}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  goodsCartCount: PropTypes.number.isRequired,
  goodsFavoriteCount: PropTypes.number.isRequired,
};

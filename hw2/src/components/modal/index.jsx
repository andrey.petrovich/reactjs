import React, { Component } from "react";
import {
  MyModalWrapper,
  MyModal,
  MyModalHeader,
  MyModalCloseBtn,
  MyModalContent,
  MyModalAction,
} from "../../styles/modal";

export class Modal extends Component {
  constructor(props) {
    super(props);
    this.header = props.header;
    this.closeButton = props.closeButton;
    this.text = props.text;
    this.action = props.action;
    this.className = props.className;
    this.setVisible = props.setVisible;
  }

  render() {
    return (
      <MyModalWrapper onClick={this.setVisible}>
        <MyModal
          className={this.className}
          onClick={(e) => e.stopPropagation()}
        >
          <MyModalHeader className={this.className}>
            <h1>{this.header}</h1>
            {this.closeButton && (
              <MyModalCloseBtn onClick={this.setVisible}>X</MyModalCloseBtn>
            )}
          </MyModalHeader>
          <MyModalContent>{this.text}</MyModalContent>
          <MyModalAction>{this.action}</MyModalAction>
        </MyModal>
      </MyModalWrapper>
    );
  }
}
import React, { Component } from "react";
import styles from "../../styles/good.module.scss";
import PropTypes from "prop-types";

import { Button } from "../button";
import { Modal } from "../modal";

export class Good extends Component {
  constructor(props) {
    super(props);
    this.item = props.item;
    this.state = {
      addToCart: false,
      goodsFavoriteId: false,
      goodsCartId: false,
    };
  }

  componentDidMount() {
    const propsValue = ["goodsFavoriteId", "goodsCartId"];

    propsValue.forEach((name) => {
      const getStorageValue = localStorage.getItem(name);
      if (getStorageValue) {
        const splitValue = getStorageValue.split(",");
        const isGood = splitValue.find((id) => this.item.id === Number(id));
        if (isGood) {
          this.setState({ [name]: true });
        }
      }
    });
  }

  render() {
    const { id, name, price, imgUrl, article, color, memory } = this.item;
    return (
      <>
        <div className={styles.GoodContainer}>
          <div className={styles.Good}>
            <div className={styles.body}>
              <div className={styles.img}>
                <img src={`./img/${imgUrl}`} alt="" />
              </div>
              <div className={styles.title}>{name}</div>
              <div className={styles.article}>
                <span className={styles.articleText}>Артикул</span>:&nbsp;
                <span className={styles.articleValue}>{article}</span>
              </div>
              <div className={styles.color}>
                <span className={styles.colorText}>Колір</span>:&nbsp;
                <span className={styles.colorValue}>{color}</span>
              </div>
              <div className={styles.memory}>
                <span className={styles.memoryText}>Об'єм пам'яті</span>:&nbsp;
                <span className={styles.memoryValue}>{memory}</span>
              </div>
            </div>
            <div className={styles.bottom}>
              <div className={styles.actions}>
                <span className={styles.cart}>
                  {!this.state.goodsCartId ? (
                    <i
                      className="fa-solid fa-circle-plus"
                      onClick={() => {
                        this.setState({ addToCart: true });
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fa-solid fa-basket-shopping"
                      onClick={() => {
                        this.setState({ goodsCartId: false });
                        this.props.removeStorageValue("goodsCartId", id);
                      }}
                    ></i>
                  )}
                </span>
                <span className={styles.favorites}>
                  {!this.state.goodsFavoriteId ? (
                    <i
                      className="fa-regular fa-star"
                      onClick={() => {
                        this.setState({ goodsFavoriteId: true });
                        this.props.addStorageValue("goodsFavoriteId", id);
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fa-solid fa-star"
                      onClick={() => {
                        this.setState({ goodsFavoriteId: false });
                        this.props.removeStorageValue("goodsFavoriteId", id);
                      }}
                    ></i>
                  )}
                </span>
              </div>
              <div className={styles.price}>
                <span className={styles.sum}>{price}</span>
                <span className={styles.currency}> грн</span>
              </div>
            </div>
          </div>
        </div>
        {this.state.addToCart && (
          <Modal
            className={"red"}
            setVisible={() => this.setState({ addtoCart: false })}
            header={<>Додати товар до кошика?</>}
            closeButton={true}
            text={
              <div className={styles.modalAddCart}>
                <div className={styles.title}>{name}</div>
                <div className={styles.price}>
                  <span className={styles.sum}>{price}</span>
                  <span className={styles.currency}> грн</span>
                </div>
              </div>
            }
            action={
              <>
                <Button
                  className={"red"}
                  text="Так"
                  backgroundColor="#b3382c"
                  onClick={() => {
                    this.setState({ goodsCartId: true });
                    this.props.addStorageValue("goodsCartId", id);
                    this.setState({ addToCart: false });
                  }}
                />
                <Button
                  className={"red"}
                  text="Ні"
                  backgroundColor="#b3382c"
                  onClick={() => this.setState({ addToCart: false })}
                />
              </>
            }
          />
        )}
      </>
    );
  }
}

Good.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    memory: PropTypes.string,
  }).isRequired,

  addStorageValue: PropTypes.func.isRequired,
  removeStorageValue: PropTypes.func.isRequired,
};

Good.defaultProps = {
  item: {
    name: "",
    price: 0,
    imgUrl: "",
    article: "",
    color: "",
    memory: "",
  },
};

import React, { Component } from "react";
import { MyButton } from "../../styles/button.js";

export class Button extends Component {
  constructor(props) {
    super(props);
    this.onClick = props.onClick;
    this.backgroundColor = props.backgroundColor;
    this.text = props.text;
    this.className = props.className;
  }

  render() {
    return (
      <MyButton
        className={this.className}
        style={{ backgroundColor: this.backgroundColor }}
        onClick={() => this.onClick()}
      >
        {this.text}
      </MyButton>
    );
  }
}